## Date and time
October 28th 2015 Thursday

## Attendees
Saumya Rawat, Anjali Shenoy, Sreeja Kamishetty, Ramya Keerthana , Dhruv Sapra

## Client
sandra reidal

## Designers 
kalpesh khatiri,kavyani reddy,chakshu,yoshita 

## Agenda
Discussion on the resource center , ibm presentation,charachter page,disclaimer,designing of objects

## Minutes
### Discussion
* Disclaimer
    * This will be shown to the player to separate serious part with fun part
* IBM Presentation.
	 
    * Polar Bear is giving a presentation of the game on November 1st  which is conducted by the IBM .
    * We need to prepare a presentation of game.
    * The presentation should include the screenshot of the game and how we developed it , IBM Bluemix deployment.
* Charachter Page Design.
    * Charachter page must be more colorful and attractive
    * We should re-design the character page
* Research Center
	* The information regarding the plastic items, facts  etc will be in the resource center 
	* It helps the player to know about the effects of plastic and about the products which are non Eco-friendly   and the consequences of the products
* Objects
	*  We need to design the objects i.e Eco-friendly and non Eco-friendly so that the player need to choose between the both 
	* The objects will be falling from the aisles   when the game starts.




## Date of the next meeting
4th November 2015

## About Our R1
*	client was so happy about our presentation .
*	she congratulated everyone for their effort

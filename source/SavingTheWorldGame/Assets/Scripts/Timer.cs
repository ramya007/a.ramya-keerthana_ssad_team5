﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour
{
	//public int lapsNumber=2;
	public float startTime=0;
	public float stageTime=0;
	private GUIStyle guiStyle = new GUIStyle();
	public Font MyFont;
	public AudioClip destroysound;
	private AudioSource audioSource;

	//public redirection obj;

	void Start()
	{
		GUI.Label (new Rect (40, 40, 150, 150), "Character Value :" + PlayerPrefs.GetInt("character"),guiStyle);

		startTime = Time.time;
		audioSource = GetComponent<AudioSource>();	
		//obj = gameObject.GetComponent<redirection> ();
	}
	
	void Update()
	{

		stageTime = 60 - (Time.time - startTime);
		if (stageTime <= 1.0f) {
			audioSource.clip = destroysound;
			audioSource.Play();
		
		}

	}
	
	void OnGUI() {
		if (stageTime <= 0) {
			//GUI.Label(new Rect(100,870, 150, 150), "Time left :p" + stageTime.ToString("F1") + "Secs",guiStyle);
			if (PlayerPrefs.GetInt("character") == 0)
				Application.LoadLevel ("oliver_checkout");
			else if (PlayerPrefs.GetInt("character") == 2)
				Application.LoadLevel ("mark_checkout");
			else if (PlayerPrefs.GetInt("character") == 1)
				Application.LoadLevel ("linda_checkout"); 
		}
			//guiStyle.fontSize = 60; 
			GUI.skin.font = MyFont;
		GUI.Box (new Rect (((Screen.width)/2 -(Screen.width)/2)+4,  ((Screen.height)/9-(Screen.height)/8) ,(Screen.width)/4,(Screen.height)/4), "<size=60>"+"Timer: "+stageTime.ToString ("F1")+"</size>"); 
		}
}
